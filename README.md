# README #

### This code creates a basic Blog using Ruby on Rails as a part of web application architectures course on coursera ###

* Create blog posts
* Allow posting comments and associating them with the posts
* Add dynamic content via AJAX
* backend: sqllite
* basic authentication

### Contact ###

* For any questions, please contact me at: labheshr[at]gmail[dot]com